# EE Footer #

A bespoke port of the News Labs footer for use in EE.

### What is this repository for? ###

* To be integrated into the EE V5 project

### How do I get set up? ###

* This assumes you are running SASS
* cd into your local ee-footer directory
* run something like
* sass --watch app/styles/index.scss:dist/styles/index.css

### Contribution guidelines ###

* Dwight Friesen stole it from News Labs

### Who do I talk to? ###

* Dwight Friesen/Matt Crider